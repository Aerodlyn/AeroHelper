HEADERS += $$PWD/Root/Utils.h \
    $$PWD/VertexEditor/VertexEditorImage.h \
    $$PWD/VertexEditor/VertexEditorWindow.h \
    $$PWD/VertexEditor/VertexEditorTable.h \
    $$PWD/VertexEditor/VertexEditorRenderedImage.h

SOURCES += $$PWD/Root/Main.cpp \
    $$PWD/Root/Utils.cpp \
    $$PWD/VertexEditor/VertexEditorImage.cpp \
    $$PWD/VertexEditor/VertexEditorWindow.cpp \
    $$PWD/VertexEditor/VertexEditorTable.cpp \
    $$PWD/VertexEditor/VertexEditorRenderedImage.cpp
